package com.example.fyp_passwordmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class StartActivity extends AppCompatActivity {

    private Button register;
    private Button login;
    private Button forgotten;
    private EditText LogEmail;
    private EditText LogPassword;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        mAuth = FirebaseAuth.getInstance();
        final FirebaseAuth fAuth = FirebaseAuth.getInstance();

        register =findViewById(R.id.register);
        login =findViewById(R.id.Reset);
        forgotten =findViewById(R.id.forgotten);
        LogEmail =findViewById(R.id.logEmail);
        LogPassword =findViewById(R.id.logPassword);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//moves the view to the register view
                startActivity(new Intent(StartActivity.this, RegisterActivity.class));
                finish();
            }
        });

        forgotten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//moves the view to the forgotten password view
                startActivity(new Intent(StartActivity.this, PassReset.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//attempts to log the user in
                String txt_email = LogEmail.getText().toString();
                String txt_password = LogPassword.getText().toString();
                if (TextUtils.isEmpty(txt_email)) {
                    LogEmail.setError("Required Field");
                    return;
                }
                if (TextUtils.isEmpty(txt_password)) {
                    LogPassword.setError("Required Field");
                    return;
                }
                else {
                    mAuth.signInWithEmailAndPassword(txt_email, txt_password).addOnCompleteListener(StartActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {

                                        if (fAuth.getCurrentUser().isEmailVerified())
                                        {
                                            finish();
                                            Toast.makeText(StartActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(StartActivity.this, MainActivity.class));
                                        } else {
                                            fAuth.signOut();
                                            Toast.makeText(StartActivity.this, "Please verify", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(StartActivity.this, StartActivity.class));
                                        }

                                    } else {
                                        Toast.makeText(StartActivity.this, "Incorrect username or password", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
    }
}