package com.example.fyp_passwordmanager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SettingsFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Button forgotten_Pass;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_settings, container, false);
        Spinner spinner = v.findViewById(R.id.visuals);
        forgotten_Pass = v.findViewById(R.id.forgotten_Pass);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.visuals, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        forgotten_Pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user.isEmailVerified())
                {
                    // user is verified, so you can finish this activity or send user to activity which you want.
                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    String email = String.valueOf(user.getEmail());
                    auth.sendPasswordResetEmail(email)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        Toast.makeText(getContext(), "Password reset Successful", Toast.LENGTH_SHORT).show();
                                       // startActivity(new Intent(getContext(), SettingsFragment.class));
                                    }
                                    else {
                                        Toast.makeText(getContext(), "Password reset unsuccessful", Toast.LENGTH_SHORT).show();
                                      //  startActivity(new Intent(getContext(), SettingsFragment.class));
                                    }
                                }
                            });
                }
                else
                {
                    // email is not verified, so just prompt the message to the user and restart this activity.
                    // NOTE: don't forget to log out the user.
                    Toast.makeText(getContext(), "Please verify", Toast.LENGTH_SHORT).show();
                 //   startActivity(new Intent(getContext(), SettingsFragment.class));
                    //restart this activity

                }
            }
        });

            return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selected = parent.getItemAtPosition(position).toString();
        if (selected == "Default");

        if (selected == "High Contrast");

        if (selected == "Deuteranopia");

        if (selected == "Protanopia");

        if (selected == "Tritanopia");

        if (selected == "Dyslexic");

        Toast.makeText(parent.getContext(), selected, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
