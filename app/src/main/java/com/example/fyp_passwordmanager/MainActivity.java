package com.example.fyp_passwordmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
private DrawerLayout drawer;  //drawer for navigation use
    private TextView UserEmail; //stores user's email
    private TextView UserName;
    private ImageView UserPic;
    private FirebaseAuth auth;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserEmail =findViewById(R.id.UserEmail);//assigns the email display point to the UserEmail variable
        UserName =findViewById(R.id.UserName);//assigns the username display point to the UserName variable
        UserPic =findViewById(R.id.UserPic);//assigns the user's picture display point to the UserPic variable


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();//gets the current active user
        if (user != null) {
            Intent intent = new Intent(MainActivity.this, StartActivity.class);//sets the view to the login page
        } else {
            String userEmail = user.getEmail();
            UserEmail.setText(userEmail); //sets the email string to the user's email
        }


        setContentView(R.layout.activity_main);//sets the content view

       Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar); //adds in a custom toolbar to the top of the screen

        drawer = findViewById(R.id.drawerLayout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this); //adds in a customer drawer

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close); //adds a toggle to open the drawer
        drawer.addDrawerListener(toggle); //listener for the drawer
        toggle.syncState();
if (savedInstanceState == null) {
    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new PasswordFragment()).commit();
    navigationView.setCheckedItem(R.id.nav_password); //sets the password button as selected
}
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
      switch (item.getItemId()) {
          case R.id.nav_password: //button to open the password fragment
              getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new PasswordFragment()).commit();
              break;
          case R.id.nav_settings: //button to open the notifications fragment
              getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new SettingsFragment()).commit();
              break;
          case R.id.nav_generation: //button to open the password generation fragment
              getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new GenerateFragment()).commit();
              break;

          case R.id.nav_hide: //button to hide all passwords
              PasswordFragment.Password_text_0.setText(PasswordFragment.Password_0_hide);
              PasswordFragment.expand_0.setVisibility(View.GONE);//hides the 0 password card as well as the 0 password

              PasswordFragment.Password_text_1.setText(PasswordFragment.Password_1_hide);
              PasswordFragment.expand_1.setVisibility(View.GONE);//hides the 1 password card as well as the 1 password

              PasswordFragment.Password_text_2.setText(PasswordFragment.Password_2_hide);
              PasswordFragment.expand_2.setVisibility(View.GONE);//hides the 2 password card as well as the 2 password

              PasswordFragment.Password_text_3.setText(PasswordFragment.Password_3_hide);
              PasswordFragment.expand_3.setVisibility(View.GONE);//hides the 3 password card as well as the 3 password

              PasswordFragment.Password_text_4.setText(PasswordFragment.Password_4_hide);
              PasswordFragment.expand_4.setVisibility(View.GONE);//hides the 4 password card as well as the 4 password

              PasswordFragment.Password_text_5.setText(PasswordFragment.Password_5_hide);
              PasswordFragment.expand_5.setVisibility(View.GONE);//hides the 5 password card as well as the 5 password

              PasswordFragment.Password_text_6.setText(PasswordFragment.Password_6_hide);
              PasswordFragment.expand_6.setVisibility(View.GONE);//hides the 6 password card as well as the 6 password

              PasswordFragment.Password_text_7.setText(PasswordFragment.Password_7_hide);
              PasswordFragment.expand_7.setVisibility(View.GONE);//hides the 7 password card as well as the 7 password

              PasswordFragment.Password_text_8.setText(PasswordFragment.Password_8_hide);
              PasswordFragment.expand_8.setVisibility(View.GONE);//hides the 8 password card as well as the 8 password

              Toast.makeText(this, "All hidden", Toast.LENGTH_SHORT).show();//sets a toast to inform the user
              break;
          case R.id.nav_logout: //button to log out
              FirebaseAuth.getInstance().signOut();
              Toast.makeText(this, "Logged Out", Toast.LENGTH_SHORT).show();
              Intent intent = new Intent(MainActivity.this, StartActivity.class);//sets the activity to the sign in activity
              this.startActivity(intent);
              break;
      }

  drawer.closeDrawer(GravityCompat.START); //closes the drawer to the left side of the screen

        return true;
    }

    public void onBackPressed() {
      if (drawer.isDrawerOpen(GravityCompat.START)) {//if the back button is pressed close the drawer
          drawer.closeDrawer(GravityCompat.START);
      } else {
          super.onBackPressed();
      }
    }
}
