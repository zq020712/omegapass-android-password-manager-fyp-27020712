package com.example.fyp_passwordmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    private EditText email;
    private EditText password;
    private EditText repassword;
    private Button register;
    private ImageButton back;

    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        repassword = findViewById(R.id.repassword);
        register = findViewById(R.id.register);
        back = findViewById(R.id.back);

        auth = FirebaseAuth.getInstance();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, StartActivity.class));
            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_email = email.getText().toString(); //moves email entyry into a string
                String txt_password = password.getText().toString(); //moves password into a string
                String txt_repassword = repassword.getText().toString(); //moves re-entered password into a string

                if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password) || TextUtils.isEmpty(txt_repassword)) {
                    Toast.makeText(RegisterActivity.this, "Empty credentials", Toast.LENGTH_SHORT).show(); //creates a toast to inform the user they haven't entered something
                } else if (txt_password.length() < 12) {
                    Toast.makeText(RegisterActivity.this, "Password must be at least 12 characters long", Toast.LENGTH_SHORT).show(); //creates a toast to inform the user they have entered a password that is too short
                } else {
                    if (txt_password.equals(txt_repassword)) {
                        Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
                        Matcher matcher = pattern.matcher(txt_password);
                        boolean isStringContainsSpecialCharacter = matcher.find();
                        if(isStringContainsSpecialCharacter) {
                            registerUser(txt_email, txt_password);
                        }
                        else {
                            Toast.makeText(RegisterActivity.this, "Password must contain at least one symbol", Toast.LENGTH_LONG).show(); //creates a toast to inform the user to add symbols to their password
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, "Passwords do not match", Toast.LENGTH_SHORT).show(); //creates a toast informing the user their passwords do not match
                    }

                }
            }
        });

    }

    private void registerUser(String email, String password) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {//creates the user profile
                if (task.isSuccessful()) {
                    sendVerificationEmail(); //sends a verification email to the user's entered email
                    finish();
                } else {
                    Toast.makeText(RegisterActivity.this, "This email address is already registered with another account", Toast.LENGTH_SHORT).show(); //informs the user their
                }
            }
        });
    }


    private void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // sends email


                            // logs the user out of the app so they can log in once verified
                            FirebaseAuth.getInstance().signOut();
                            startActivity(new Intent(RegisterActivity.this, StartActivity.class));
                            Toast.makeText(RegisterActivity.this, "Please check emails for verification", Toast.LENGTH_LONG).show(); //creates a toast to inform the user to check their email for verification
                            finish();
                        } else {

                            //restart the activity
                            overridePendingTransition(0, 0);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());

                        }
                    }
                });
    }

}