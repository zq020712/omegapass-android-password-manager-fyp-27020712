package com.example.fyp_passwordmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class PassReset extends AppCompatActivity {

    EditText ResEmail;//email field
    Button Reset;//reset button

    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_reset);//sets the content view to the rest page

        ResEmail = findViewById(R.id.ResEmail);//assigns the ResEmail field to the ResEmail variable
        Reset = findViewById(R.id.Reset);//assigns the Reset button to the Reset variable

        firebaseAuth = FirebaseAuth.getInstance();

        Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (TextUtils.isEmpty((CharSequence) ResEmail) || TextUtils.isEmpty((CharSequence) ResEmail) || TextUtils.isEmpty((CharSequence) ResEmail)) {
//                    Toast.makeText(PassReset.this, "Empty credentials", Toast.LENGTH_SHORT).show(); //creates a toast to inform the user they haven't entered something
//                }
//                else {
                    firebaseAuth.sendPasswordResetEmail(ResEmail.getText().toString())//sends a reset password email
                            .addOnCompleteListener(new OnCompleteListener<Void>() {

                                @Override
                                public void onComplete(Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(PassReset.this, "Password reset email sent", Toast.LENGTH_SHORT).show();//informs the user the email was sent
                                        startActivity(new Intent(PassReset.this, StartActivity.class));
                                    }
                                    else {
                                        Toast.makeText(PassReset.this, "Password reset unsuccessful", Toast.LENGTH_SHORT).show();//informs the user the email was not sent
                                    }
                                }
                            });
//                }
            }
        });
    }
}