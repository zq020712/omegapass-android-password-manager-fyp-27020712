package com.example.fyp_passwordmanager;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;

import java.util.Random;

public class GenerateFragment extends Fragment {

    ////////////////////
    //Generate strings
    ////////////////////
    ImageButton Generate_copy; //copy button for the generation fragment
    Button Generate_button; //generate button for the generation fragment
    TextView Generate_text; //text view to display the generated password for the generation fragment
    public String Generated_text = "Generate a password!"; //default text for the text view
    public static String Generator_characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}|;:',<.>/?"; //string from which the generated password's characters are picked
    Random Generate_random = new Random(); //random string to generate password from
    String result;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        final View v = inflater.inflate(R.layout.fragment_generate, container, false);

        ////////////////////////
        //password_0 UI elements
        ////////////////////////
        Generate_text = (TextView) v.findViewById(R.id.Generate_text_view); //text view stored for generation
        Generate_copy = (ImageButton) v.findViewById(R.id.Generate_copy); //image button stored for generation
        Generate_button = (Button) v.findViewById(R.id.Generate_button); //Button stored for generation
        Generate_text.setText(Generated_text); //sets the text of the text view to the generated_text variable


        ////////////////////
        //password_0 buttons
        ////////////////////


        Generate_copy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE); //creates a variable for the clipboard to be copied to
                ClipData clip = ClipData.newPlainText("Password_0", Generated_text.toString()); //copies the contents of Password_0 to the variable
                clipboard.setPrimaryClip(clip);//adds the copied text to the phone's clipboard

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show(); //creates a toast message to inform the user they have copied their new password

            }
        });

        Generate_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                for (int counter = 0; counter < 21; counter++) {
                    Generated_text = stringBuilder.append(Generator_characters.charAt(Generate_random.nextInt(Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Generate_text.setText(Generated_text);//sets the text field as the randomly generated string

            }
        });

        return v;
    }
}









