package com.example.fyp_passwordmanager;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils
{

    private static final byte[] KEY =
            new byte[]{'o', 'm', 'e', 'g', 'a', 'p', 'a', 's', 's', 'r', 'o', 'x','m','a','n','!'};


    public static String Encryption(String cleartext) //function to take the key and the plain text for encryption
            throws Exception {
        byte[] RAWKey = RAWKeyGet();
        byte[] resultant = Encryption(RAWKey, cleartext.getBytes());
        return tohexadecimal(resultant);
    }

    public static String Decryption(String Encrypted)//function to take the cipher text for encryption
            throws Exception {

        byte[] enc = ToBytes(Encrypted);
        byte[] resultant = Decryption(enc);
        return new String(resultant);
    }

    private static byte[] RAWKeyGet() throws Exception {//retrieves the Raw Key for use in encryption/decryption
        SecretKey Key = new SecretKeySpec(KEY, "AES");
        byte[] raw = Key.getEncoded();
        return raw;
    }

    private static byte[] Encryption(byte[] raw, byte[] clear) throws Exception {//function to run the text through the AES function from the crypto library to encrypt the plain text
        SecretKey sKeySpec = new SecretKeySpec(raw, "AES");
        Cipher CIPHER = Cipher.getInstance("AES");
        CIPHER.init(Cipher.ENCRYPT_MODE, sKeySpec);
        byte[] Encrypted = CIPHER.doFinal(clear);
        return Encrypted;
    }

    private static byte[] Decryption(byte[] Encrypted)//function to run the text through the AES function from the crypto library to decrypt the cipher text
            throws Exception {
        SecretKey sKeySpec = new SecretKeySpec(KEY, "AES");
        Cipher CIPHER = Cipher.getInstance("AES");
        CIPHER.init(Cipher.DECRYPT_MODE, sKeySpec);
        byte[] Decryptioned = CIPHER.doFinal(Encrypted);
        return Decryptioned;
    }

    public static byte[] ToBytes(String hexadecimalString) {//converts the plain/cipher text to bytes
        int len = hexadecimalString.length() / 2;
        byte[] resultant = new byte[len];
        for (int i = 0; i < len; i++)
            resultant[i] = Integer.valueOf(hexadecimalString.substring(2 * i, 2 * i + 2),
                    16).byteValue();
        return resultant;
    }

    public static String tohexadecimal(byte[] buf) {//converts the plain/cipher text to hexadecimal
        if (buf == null)
            return "";
        
        StringBuffer resultant = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++) {
            appendhexadecimal(resultant, buf[i]);
        }
        return resultant.toString();
    }

    private final static String hexadecimal = "0123456789ABCDEF";

    private static void appendhexadecimal(StringBuffer sb, byte b) {//appends the hexadecimal string to meet the AES block size.
        sb.append(hexadecimal.charAt((b >> 4) & 0x0f)).append(hexadecimal.charAt(b & 0x0f));
    }
}