package com.example.fyp_passwordmanager;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.TwoStatePreference;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.transition.AutoTransition;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.text.BreakIterator;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static android.content.Context.MODE_PRIVATE;
import static androidx.core.content.ContextCompat.getSystemService;

public class PasswordFragment extends Fragment {

    public String Saver;
    public String Loader;
    public String Num;
    public String carrier = "";

    ////////////////////
    //password_0 strings
    ////////////////////
    ImageButton copyTextButton_0;//variable for the 0 copy button
    Button editTextButton_0;//variable for the 0 edit button
    public static ToggleButton changeTextButton_0;//variable for the 0 hide button
    public static TextView Password_text_0;//variable for the 0 password text view
    String Password_0 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_0_hide = "*********";//string to hide the password
    String password_edit_0 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_0;//variable for the expand_0 card
    ImageButton icon_0;//button to open the above card
    private static final String Password_File_0 = "password_0.txt";//password storage file fo password 0
    Button Generate_0;//generate password 0 button

    ////////////////////
    //password_1 strings
    ////////////////////
    ImageButton copyTextButton_1;//variable for the 1 copy button
    Button editTextButton_1;//variable for the 1 edit button
    public static ToggleButton changeTextButton_1;//variable for the 1 hide button
    public static TextView Password_text_1;//variable for the 1 password text view
    String Password_1 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_1_hide = "*********";//string to hide the password
    String password_edit_1 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_1;//variable for the expand_1 card
    ImageButton icon_1;//button to open the above card
    private static final String Password_File_1 = "password_1.txt";//password storage file fo password 1
    Button Generate_1;//generate password 1 button

    ////////////////////
    //password_2 strings
    ////////////////////
    ImageButton copyTextButton_2;//variable for the 2 copy button
    Button editTextButton_2;//variable for the 2 edit button
    public static ToggleButton changeTextButton_2;//variable for the 2 hide button
    public static TextView Password_text_2;//variable for the 2 password text view
    String Password_2 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_2_hide = "*********";//string to hide the password
    String password_edit_2 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_2;//variable for the expand_2 card
    ImageButton icon_2;//button to open the above card
    private static final String Password_File_2 = "password_2.txt";//password storage file for password 2
    Button Generate_2;//generate password 2 button

    ////////////////////
    //password_3 strings
    ////////////////////
    ImageButton copyTextButton_3;//variable for the 3 copy button
    Button editTextButton_3;//variable for the 3 edit button
    public static ToggleButton changeTextButton_3;//variable for the 3 hide button
    public static TextView Password_text_3;//variable for the 3 password text view
    String Password_3 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_3_hide = "*********";//string to hide the password
    String password_edit_3 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_3;//variable for the expand_3 card
    ImageButton icon_3;//button to open the above card
    private static final String Password_File_3 = "password_3.txt";//password storage file fo password 3
    Button Generate_3;//generate password 3 button

    ////////////////////
    //password_4 strings
    ////////////////////
    ImageButton copyTextButton_4;//variable for the 4 copy button
    Button editTextButton_4;//variable for the 4 edit button
    public static ToggleButton changeTextButton_4;//variable for the 4 hide button
    public static TextView Password_text_4;//variable for the 4 password text view
    String Password_4 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_4_hide = "*********";//string to hide the password
    String password_edit_4 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_4;//variable for the expand_4 card
    ImageButton icon_4;//button to open the above card
    private static final String Password_File_4 = "password_4.txt";//password storage file fo password 4
    Button Generate_4;//generate password 4 button

    ////////////////////
    //password_5 strings
    ////////////////////
    ImageButton copyTextButton_5;//variable for the 5 copy button
    Button editTextButton_5;//variable for the 5 edit button
    public static ToggleButton changeTextButton_5;//variable for the 5 hide button
    public static TextView Password_text_5;//variable for the 5 password text view
    String Password_5 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_5_hide = "*********";//string to hide the password
    String password_edit_5 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_5;//variable for the expand_5 card
    ImageButton icon_5;//button to open the above card
    private static final String Password_File_5 = "password_5.txt";//password storage file fo password 5
    Button Generate_5;//generate password 5 button

    ////////////////////
    //password_6 strings
    ////////////////////
    ImageButton copyTextButton_6;//variable for the 6 copy button
    Button editTextButton_6;//variable for the 6 edit button
    public static ToggleButton changeTextButton_6;//variable for the 6 hide button
    public static TextView Password_text_6;//variable for the 6 password text view
    String Password_6 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_6_hide = "*********";//string to hide the password
    String password_edit_6 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_6;//variable for the expand_6 card
    ImageButton icon_6;//button to open the above card
    private static final String Password_File_6 = "password_6.txt";//password storage file fo password 6
    Button Generate_6;//generate password 6 button

    ////////////////////
    //password_7 strings
    ////////////////////
    ImageButton copyTextButton_7;//variable for the 7 copy button
    Button editTextButton_7;//variable for the 7 edit button
    public static ToggleButton changeTextButton_7;//variable for the 7 hide button
    public static TextView Password_text_7;//variable for the 7 password text view
    String Password_7 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_7_hide = "*********";//string to hide the password
    String password_edit_7 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_7;//variable for the expand_7 card
    ImageButton icon_7;//button to open the above card
    private static final String Password_File_7 = "password_7.txt";//password storage file fo password 7
    Button Generate_7;//generate password 7 button

    ////////////////////
    //password_8 strings
    ////////////////////
    ImageButton copyTextButton_8;//variable for the 8 copy button
    Button editTextButton_8;//variable for the 8 edit button
    public static ToggleButton changeTextButton_8;//variable for the 8 hide button
    public static TextView Password_text_8;//variable for the 8 password text view
    String Password_8 = "Please Enter A Password";//basic string to ask the user to enter a password
    public static String Password_8_hide = "*********";//string to hide the password
    String password_edit_8 = "BROKEN";//error handling variable
    public static ConstraintLayout expand_8;//variable for the expand_8 card
    ImageButton icon_8;//button to open the above card
    private static final String Password_File_8 = "password_8.txt";//password storage file fo password 8
    Button Generate_8;//generate password 8 button

    public void showAddItemDialog_0(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_0.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_0 = String.valueOf(Password_text_0.getText());
                        Saver = Password_0;//moves the password text over to the saver variable for use
                        Num = "0";//sets the num variable to 0 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_1(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Password_text_1.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_1 = String.valueOf(Password_text_1.getText());
                        Saver = Password_1;//moves the password text over to the saver variable for use
                        Num = "1";//sets the num variable to 1 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_2(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_2.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_2 = String.valueOf(Password_text_2.getText());
                        Saver = Password_2;//moves the password text over to the saver variable for use
                        Num = "2";//sets the num variable to 2 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_3(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_3.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_3 = String.valueOf(Password_text_3.getText());
                        Saver = Password_3;//moves the password text over to the saver variable for use
                        Num = "3";//sets the num variable to 3 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_4(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_4.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_4 = String.valueOf(Password_text_4.getText());
                        Saver = Password_4;//moves the password text over to the saver variable for use
                        Num = "4";//sets the num variable to 4 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_5(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_5.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_5 = String.valueOf(Password_text_5.getText());
                        Saver = Password_5;//moves the password text over to the saver variable for use
                        Num = "5";//sets the num variable to 5 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_6(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_6.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_6 = String.valueOf(Password_text_6.getText());
                        Saver = Password_6;//moves the password text over to the saver variable for use
                        Num = "6";//sets the num variable to 6 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_7(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_7.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_7 = String.valueOf(Password_text_7.getText());
                        Saver = Password_7;//moves the password text over to the saver variable for use
                        Num = "7";//sets the num variable to 7 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void showAddItemDialog_8(Context c) {//edit password dialog
        final EditText password_Entry= new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Edit password")
                .setMessage("Edit password")
                .setView(password_Entry)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Password_text_8.setText(password_Entry.getText());//sets the text of the password_text field to the entered text
                        Password_8 = String.valueOf(Password_text_8.getText());
                        Saver = Password_8;//moves the password text over to the saver variable for use
                        Num = "8";//sets the num variable to 8 to inform the save() function
                        save();//saves the text to an encrypted text file
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        final View v = inflater.inflate(R.layout.fragment_passwords, container, false);//inflates the password fragment


        ////////////////////////
        //password_0 UI elements
        ////////////////////////
        Password_text_0 = (TextView)v.findViewById(R.id.Password_text_0);
       changeTextButton_0 = (ToggleButton)v.findViewById(R.id.reveal_0);
        copyTextButton_0 = (ImageButton)v.findViewById(R.id.copy_0);
        editTextButton_0 = (Button)v.findViewById(R.id.edit_0);
        Password_text_0.setText(Password_0_hide);
        expand_0 = (ConstraintLayout)v.findViewById(R.id.expand_0);
        icon_0 = (ImageButton)v.findViewById(R.id.icon_0);
        Generate_0 = (Button)v.findViewById(R.id.Generate_0);

        ////////////////////////
        //password_1 UI elements
        ////////////////////////
        Password_text_1 = (TextView) v.findViewById(R.id.Password_text_1);
        final ToggleButton changeTextButton_1 = (ToggleButton)v.findViewById(R.id.reveal_1);
        copyTextButton_1 = (ImageButton)v.findViewById(R.id.copy_1);
        editTextButton_1 = (Button)v.findViewById(R.id.edit_1);
        Password_text_1.setText(Password_1_hide);
        expand_1 = (ConstraintLayout)v.findViewById(R.id.expand_1);
        icon_1 = (ImageButton)v.findViewById(R.id.icon_1);
        Generate_1 = (Button)v.findViewById(R.id.Generate_1);

        ////////////////////////
        //password_2 UI elements
        ////////////////////////
        Password_text_2 = (TextView) v.findViewById(R.id.Password_text_2);
        final ToggleButton changeTextButton_2 = (ToggleButton)v.findViewById(R.id.reveal_2);
        copyTextButton_2 = (ImageButton)v.findViewById(R.id.copy_2);
        editTextButton_2 = (Button)v.findViewById(R.id.edit_2);
        Password_text_2.setText(Password_2_hide);
        expand_2 = (ConstraintLayout)v.findViewById(R.id.expand_2);
        icon_2 = (ImageButton)v.findViewById(R.id.icon_2);
        Generate_2 = (Button)v.findViewById(R.id.Generate_2);

        ////////////////////////
        //password_3 UI elements
        ////////////////////////
        Password_text_3 = (TextView) v.findViewById(R.id.Password_text_3);
        final ToggleButton changeTextButton_3 = (ToggleButton)v.findViewById(R.id.reveal_3);
        copyTextButton_3 = (ImageButton)v.findViewById(R.id.copy_3);
        editTextButton_3 = (Button)v.findViewById(R.id.edit_3);
        Password_text_3.setText(Password_3_hide);
        expand_3 = (ConstraintLayout)v.findViewById(R.id.expand_3);
        icon_3 = (ImageButton)v.findViewById(R.id.icon_3);
        Generate_3 = (Button)v.findViewById(R.id.Generate_3);

        ////////////////////////
        //password_4 UI elements
        ////////////////////////
        Password_text_4 = (TextView) v.findViewById(R.id.Password_text_4);
        final ToggleButton changeTextButton_4 = (ToggleButton)v.findViewById(R.id.reveal_4);
        copyTextButton_4 = (ImageButton)v.findViewById(R.id.copy_4);
        editTextButton_4 = (Button)v.findViewById(R.id.edit_4);
        Password_text_4.setText(Password_4_hide);
        expand_4 = (ConstraintLayout)v.findViewById(R.id.expand_4);
        icon_4 = (ImageButton)v.findViewById(R.id.icon_4);
        Generate_4 = (Button)v.findViewById(R.id.Generate_4);

        ////////////////////////
        //password_5 UI elements
        ////////////////////////
        Password_text_5 = (TextView) v.findViewById(R.id.Password_text_5);
        final ToggleButton changeTextButton_5 = (ToggleButton)v.findViewById(R.id.reveal_5);
        copyTextButton_5 = (ImageButton)v.findViewById(R.id.copy_5);
        editTextButton_5 = (Button)v.findViewById(R.id.edit_5);
        Password_text_5.setText(Password_5_hide);
        expand_5 = (ConstraintLayout)v.findViewById(R.id.expand_5);
        icon_5 = (ImageButton)v.findViewById(R.id.icon_5);
        Generate_5 = (Button)v.findViewById(R.id.Generate_5);

        ////////////////////////
        //password_6 UI elements
        ////////////////////////
        Password_text_6 = (TextView) v.findViewById(R.id.Password_text_6);
        final ToggleButton changeTextButton_6 = (ToggleButton)v.findViewById(R.id.reveal_6);
        copyTextButton_6 = (ImageButton)v.findViewById(R.id.copy_6);
        editTextButton_6 = (Button)v.findViewById(R.id.edit_6);
        Password_text_6.setText(Password_6_hide);
        expand_6 = (ConstraintLayout)v.findViewById(R.id.expand_6);
        icon_6 = (ImageButton)v.findViewById(R.id.icon_6);
        Generate_6 = (Button)v.findViewById(R.id.Generate_6);

        ////////////////////////
        //password_7 UI elements
        ////////////////////////
        Password_text_7 = (TextView) v.findViewById(R.id.Password_text_7);
        final ToggleButton changeTextButton_7 = (ToggleButton)v.findViewById(R.id.reveal_7);
        copyTextButton_7 = (ImageButton)v.findViewById(R.id.copy_7);
        editTextButton_7 = (Button)v.findViewById(R.id.edit_7);
        Password_text_7.setText(Password_7_hide);
        expand_7 = (ConstraintLayout)v.findViewById(R.id.expand_7);
        icon_7 = (ImageButton)v.findViewById(R.id.icon_7);
        Generate_7 = (Button)v.findViewById(R.id.Generate_7);

        ////////////////////////
        //password_8 UI elements
        ////////////////////////
        Password_text_8 = (TextView) v.findViewById(R.id.Password_text_8);
        final ToggleButton changeTextButton_8 = (ToggleButton)v.findViewById(R.id.reveal_8);
        copyTextButton_8 = (ImageButton)v.findViewById(R.id.copy_8);
        editTextButton_8 = (Button)v.findViewById(R.id.edit_8);
        Password_text_8.setText(Password_8_hide);
        expand_8 = (ConstraintLayout)v.findViewById(R.id.expand_8);
        icon_8 = (ImageButton)v.findViewById(R.id.icon_8);
        Generate_8 = (Button)v.findViewById(R.id.Generate_8);

        expand_0.setVisibility(View.GONE);//hides the expand_0 card
        expand_1.setVisibility(View.GONE);//hides the expand_1 card
        expand_2.setVisibility(View.GONE);//hides the expand_2 card
        expand_3.setVisibility(View.GONE);//hides the expand_3 card
        expand_4.setVisibility(View.GONE);//hides the expand_4 card
        expand_5.setVisibility(View.GONE);//hides the expand_5 card
        expand_6.setVisibility(View.GONE);//hides the expand_6 card
        expand_7.setVisibility(View.GONE);//hides the expand_7 card
        expand_8.setVisibility(View.GONE);//hides the expand_8 card


        ////////////////////
        //password_0 buttons
        ////////////////////

        icon_0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_0.getVisibility()==View.GONE)
                {
                    expand_0.setVisibility(View.VISIBLE);//makes the expand_0 card to visible
                }
                else
                {
                    expand_0.setVisibility(View.GONE);//hides the expand_0 card
                    Password_text_0.setText(Password_0_hide);//hides the password text
                    changeTextButton_0.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_0.isChecked())
                {
                    Num = "0";//sets the variable Num to 0
                    load(getView());//calls the load() function
                    Password_0 = carrier;//sets password_0 to the contents of the carrier variable
                    Password_text_0.setText(Password_0);
                }
                else
                {
                    Password_text_0.setText(Password_0_hide);//hides the text in the password field
                    Password_0 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "0";
                load(getView());
                Password_0 = carrier;

                ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Password_0", Password_0.toString());//copies the password to the user's clipboard
                clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();
                Password_0 = password_edit_0;

            }
        });

        editTextButton_0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_0(getActivity());//calls the showAddItemDialog_0() function
                Saver = Password_0;
                Num = "0";
                Password_0 = password_edit_0;
                changeTextButton_0.setChecked(true);//sets the revel button to checked

            }
        });
        Generate_0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //builds a new 21 character long string
                String Generated_0 = "";
                Random Generate_random_0 = new Random(); //the random string being generated from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_0 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_0.nextInt(GenerateFragment.Generator_characters.length()))).toString();//generates a completley random 21 character string for use
                }
                Password_text_0.setText(Generated_0);
                Password_0 = String.valueOf(Password_text_0.getText());//gets the value of the password_text field and put it into the password variable
                Saver = Password_0;
                Num = "0";
                save();//calls the save() function.
                changeTextButton_0.setChecked(true);
            }
        });

        ////////////////////
        //password_1 buttons
        ////////////////////

        icon_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_1.getVisibility()==View.GONE)
                {
                    expand_1.setVisibility(View.VISIBLE);//makes the expand_1 card to visible
                }
                else
                {
                    expand_1.setVisibility(View.GONE);//hides the expand_1 card
                    Password_text_1.setText(Password_1_hide);//hides the password text
                    changeTextButton_1.setChecked(false);//set the change text button to false

                }
            }
        });

        changeTextButton_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_1.isChecked())
                {
                    Num = "1"; //sets the variable Num to 1
                    load(getView()); //calls the load() function
                    Password_1 = carrier;//sets password_0 to the contents of the carrier variable
                    Password_text_1.setText(Password_1);
                }
                else
                {
                    Password_text_1.setText(Password_1_hide);//hides the text in the password field
                    Password_1 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "1";
                load(getView());
                Password_1 = carrier;

                  ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_1", Password_1.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();
                Password_1 = password_edit_0;

            }
        });

        editTextButton_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_1(getActivity());
                Saver = Password_1;
                Num = "1";
                Password_1 = password_edit_1;
                changeTextButton_1.setChecked(true);
            }
        });

        Generate_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_1 = "";
                Random Generate_random_1 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_1 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_1.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_1.setText(Generated_1);
                Password_1 = String.valueOf(Password_text_1.getText());
                Saver = Password_1;
                Num = "1";
                save();
                changeTextButton_1.setChecked(true);
            }
        });

        ////////////////////
        //password_2 buttons
        ////////////////////

        icon_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_2.getVisibility()==View.GONE)
                {
                    expand_2.setVisibility(View.VISIBLE);//makes the expand_2 card to visible
                }
                else
                {
                    expand_2.setVisibility(View.GONE);//hides the expand_2 card
                    Password_text_2.setText(Password_2_hide);//hides the password text
                    changeTextButton_2.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_2.isChecked())
                {
                    Num = "2";//sets the variable Num to 2
                    load(getView());//calls the load() function
                    Password_2 = carrier;//sets password_2 to the contents of the carrier variable
                    Password_text_2.setText(Password_2);
                }
                else
                {
                    Password_text_2.setText(Password_2_hide);//hides the text in the password field
                    Password_2 = password_edit_0;//deletes the password within the variable

                }
            }
        });

        copyTextButton_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "2";
                load(getView());
                Password_2 = carrier;

                    ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_2", Password_2.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();

                Password_2 = password_edit_0;

            }
        });

        editTextButton_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_2(getActivity());
                Password_2 = password_edit_2;
                changeTextButton_2.setChecked(true);
            }
        });

        Generate_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_2 = "";
                Random Generate_random_2 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_2 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_2.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_2.setText(Generated_2);
                Password_2 = String.valueOf(Password_text_2.getText());
                Saver = Password_2;
                Num = "2";
                save();
                changeTextButton_2.setChecked(true);
            }
        });

        ////////////////////
        //password_3 buttons
        ////////////////////

        icon_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_3.getVisibility()==View.GONE)
                {
                    expand_3.setVisibility(View.VISIBLE); //makes the expand_3 card to visible
                }
                else
                {
                    expand_3.setVisibility(View.GONE);//hides the expand_3 card
                    Password_text_3.setText(Password_3_hide);//hides the password text
                    changeTextButton_3.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_3.isChecked())
                {
                    Num = "3"; //sets the variable Num to 3
                    load(getView());//calls the load() function
                    Password_3 = carrier;//sets password_3 to the contents of the carrier variable
                    Password_text_3.setText(Password_3);
                }
                else
                {
                    Password_text_3.setText(Password_3_hide);//hides the text in the password field
                    Password_3 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "3";
                load(getView());
                Password_3 = carrier;

                    ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_3", Password_3.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);;

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();

                Password_3 = password_edit_0;


            }
        });

        editTextButton_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_3(getActivity());
                Password_3 = password_edit_3;
                Num = "3";
                changeTextButton_3.setChecked(true);
            }
        });

        Generate_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_3 = "";
                Random Generate_random_3 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_3 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_3.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_3.setText(Generated_3);
                Password_3 = String.valueOf(Password_text_3.getText());
                Saver = Password_3;
                Num = "3";
                save();
                changeTextButton_3.setChecked(true);
            }
        });

        ////////////////////
        //password_4 buttons
        ////////////////////

        icon_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_4.getVisibility()==View.GONE)
                {
                    expand_4.setVisibility(View.VISIBLE); //makes the expand_4 card to visible
                }
                else
                {
                    expand_4.setVisibility(View.GONE);//hides the expand_4 card
                    Password_text_4.setText(Password_4_hide);//hides the password text
                    changeTextButton_4.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_4.isChecked())
                {
                    Num = "4"; //sets the variable Num to 4
                    load(getView());//calls the load() function
                    Password_4 = carrier;//sets password_4 to the contents of the carrier variable
                    Password_text_4.setText(Password_4);
                }
                else
                {
                    Password_text_4.setText(Password_4_hide);//hides the text in the password field
                    Password_4 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "4";
                load(getView());
                Password_4 = carrier;

                    ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_4", Password_4.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();

                Password_4 = password_edit_0;
            }
        });

        editTextButton_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_4(getActivity());
                Password_4 = password_edit_4;
                Num = "4";
                changeTextButton_4.setChecked(true);
            }
        });

        Generate_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_4 = "";
                Random Generate_random_4 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_4 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_4.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_4.setText(Generated_4);
                Password_4 = String.valueOf(Password_text_4.getText());
                Saver = Password_4;
                Num = "4";
                save();
                changeTextButton_4.setChecked(true);
            }
        });

        ////////////////////
        //password_5 buttons
        ////////////////////

        icon_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_5.getVisibility()==View.GONE)
                {
                    expand_5.setVisibility(View.VISIBLE); //makes the expand_5 card to visible
                }
                else
                {
                    expand_5.setVisibility(View.GONE);//hides the expand_5 card
                    Password_text_5.setText(Password_5_hide);//hides the password text
                    changeTextButton_5.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_5.isChecked())
                {
                    Num = "5"; //sets the variable Num to 5
                    load(getView());//calls the load() function
                    Password_5 = carrier;//sets password_5 to the contents of the carrier variable
                    Password_text_5.setText(Password_5);
                }
                else
                {
                    Password_text_5.setText(Password_5_hide);//hides the text in the password field
                    Password_5 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "5";
                load(getView());
                Password_5 = carrier;

                    ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_5", Password_5.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();

                Password_5 = password_edit_0;
            }
        });

        editTextButton_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_5(getActivity());
                Password_5 = password_edit_5;
                Num = "5";
                changeTextButton_5.setChecked(true);
            }
        });

        Generate_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_5 = "";
                Random Generate_random_5 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_5 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_5.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_5.setText(Generated_5);
                Password_5 = String.valueOf(Password_text_5.getText());
                Saver = Password_5;
                Num = "5";
                save();
                changeTextButton_5.setChecked(true);
            }
        });

        ////////////////////
        //password_6 buttons
        ////////////////////

        icon_6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_6.getVisibility()==View.GONE)
                {
                    expand_6.setVisibility(View.VISIBLE); //makes the expand_6 card to visible
                }
                else
                {
                    expand_6.setVisibility(View.GONE);//hides the expand_6 card
                    Password_text_6.setText(Password_6_hide);//hides the password text
                    changeTextButton_6.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_6.isChecked())
                {
                    Num = "6"; //sets the variable Num to 6
                    load(getView());//calls the load() function
                    Password_6 = carrier;//sets password_6 to the contents of the carrier variable
                    Password_text_6.setText(Password_6);
                }
                else
                {
                    Password_text_6.setText(Password_6_hide);//hides the text in the password field
                    Password_6 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "6";
                load(getView());
                Password_6 = carrier;

                    ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_6", Password_6.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();

                Password_6 = password_edit_0;
            }
        });

        editTextButton_6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_6(getActivity());
                Password_6 = password_edit_6;
                Num = "6";
                changeTextButton_6.setChecked(true);
            }
        });

        Generate_6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_6 = "";
                Random Generate_random_6 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_6 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_6.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_6.setText(Generated_6);
                Password_6 = String.valueOf(Password_text_6.getText());
                Saver = Password_6;
                Num = "6";
                save();
                changeTextButton_6.setChecked(true);
            }
        });

        ////////////////////
        //password_7 buttons
        ////////////////////

        icon_7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_7.getVisibility()==View.GONE)
                {
                    expand_7.setVisibility(View.VISIBLE); //makes the expand_7 card to visible
                }
                else
                {
                    expand_7.setVisibility(View.GONE);//hides the expand_7 card
                    Password_text_7.setText(Password_7_hide);//hides the password text
                    changeTextButton_7.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_7.isChecked())
                {
                    Num = "7"; //sets the variable Num to 7
                    load(getView());//calls the load() function
                    Password_7 = carrier;//sets password_7 to the contents of the carrier variable
                    Password_text_7.setText(Password_7);
                }
                else
                {
                    Password_text_7.setText(Password_7_hide);//hides the text in the password field
                    Password_7 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "7";
                load(getView());
                Password_7 = carrier;

                    ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_7", Password_7.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();

                Password_7 = password_edit_0;
            }
        });

        editTextButton_7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_7(getActivity());
                Password_7 = password_edit_7;
                Num = "7";
                changeTextButton_7.setChecked(true);
            }
        });

        Generate_7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_7 = "";
                Random Generate_random_7 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_7 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_7.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_7.setText(Generated_7);
                Password_7 = String.valueOf(Password_text_7.getText());
                Saver = Password_7;
                Num = "7";
                save();
                changeTextButton_7.setChecked(true);
            }
        });


        ////////////////////
        //password_8 buttons
        ////////////////////

        icon_8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (expand_8.getVisibility()==View.GONE)
                {
                    expand_8.setVisibility(View.VISIBLE); //makes the expand_8 card to visible
                }
                else
                {
                    expand_8.setVisibility(View.GONE);//hides the expand_8 card
                    Password_text_8.setText(Password_8_hide);//hides the password text
                    changeTextButton_8.setChecked(false);//set the change text button to false
                }
            }
        });

        changeTextButton_8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (changeTextButton_8.isChecked())
                {
                    Num = "8"; //sets the variable Num to 8
                    load(getView());//calls the load() function
                    Password_8 = carrier;//sets password_8 to the contents of the carrier variable
                    Password_text_8.setText(Password_8);
                }
                else
                {
                    Password_text_8.setText(Password_8_hide);//hides the text in the password field
                    Password_8 = password_edit_0;//deletes the password within the variable
                }
            }
        });

        copyTextButton_8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Num = "8";
                load(getView());
                Password_8 = carrier;

                    ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Password_8", Password_8.toString());//copies the password to the user's clipboard
                    clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), "Copied!", Toast.LENGTH_SHORT).show();

                Password_8 = password_edit_0;
            }
        });

        editTextButton_8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAddItemDialog_8(getActivity());
                Password_8 = password_edit_8;
                Num = "8";
                changeTextButton_8.setChecked(true);
            }
        });

        Generate_8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                StringBuilder stringBuilder = new StringBuilder(21); //new string builder of 21 character length
                String Generated_8 = "";
                Random Generate_random_8 = new Random(); //random string to generate password from
                for (int counter = 0; counter < 21; counter++) {
                    Generated_8 = stringBuilder.append(GenerateFragment.Generator_characters.charAt(Generate_random_8.nextInt(GenerateFragment.Generator_characters.length()))).toString();//randomly generates a 21 character long string of characters
                }
                Password_text_8.setText(Generated_8);
                Password_8 = String.valueOf(Password_text_8.getText());
                Saver = Password_8;
                Num = "8";
                save();
                changeTextButton_8.setChecked(true);
            }
        });

        return v;
    }

    public void save() {
        final String FILE_NAME = "Password_"+ Num +"_.txt";
        FileOutputStream fos = null;
        String encrypted = "";
        String sourceStr = Saver;
        try {
                encrypted = AESUtils.Encryption(sourceStr);
                Log.d("TEST", "encrypted:" + encrypted);

            fos = getActivity().openFileOutput(FILE_NAME, MODE_PRIVATE);
            fos.write(encrypted.getBytes());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void load(View v) {
        final String FILE_NAME = "Password_"+ Num +"_.txt";
        final String fieldID = "Password_text_"+ Num;
        final String stringID = "Password_"+ Num;

        TextView Loading;
        int resID = getResources().getIdentifier(stringID, "id", getActivity().getPackageName());
        Loading = (TextView)v.findViewById(resID);

        String encrypted = "";
        String decrypted = "";

        FileInputStream fis = null;
        try {
            fis = getActivity().openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }

            encrypted = (sb.toString());

            decrypted = AESUtils.Decryption(encrypted);
            Log.d("TEST", "decrypted:" + decrypted);
            carrier = decrypted;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
